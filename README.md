# RMD Data Model

This repo describes the data model for a blockchain-enabled Regulatory Monitoring Dashboard via a JSON-Schema descritption.  The schema is complex, so it is defined in pieces and assembled via `jq` pipelines coordinated by a Makefile.

This repo also contains some Proof of Concept python code for generating RMD data from either real or mock event data.

## Constructing the Schema

### Dependencies

- GNU Make
- jq

(Need installation instructions)

### Build process

Just run make!

## Resources:

Check example and real data blobs against the schema with: https://www.jsonschemavalidator.net/
