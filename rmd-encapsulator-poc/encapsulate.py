#!/usr/bin/env python3
import json
import sys
from ipfs_cid import cid_sha256_hash

obj_type_map = {
    "party": "party_or_entity",
    "game": "game_event_details",
    "report": "game_end_report"
}

def encapsulate_dict(obj, obj_type):
    assert type(obj) == dict
    canon = get_canonical(obj)
    cid = get_cid(canon)

    out_obj = {
        'data_type': 'RMD_DATA_OBJECT',
        'object_type': obj_type,
        'object_id': cid,
        'object': obj
    }

    #out = json.dumps(out_obj, separators=(',',':'), sort_keys=False)

    return out_obj

def get_canonical(in_obj):
    """ Encode a dict into canonical JSON bytes.  Canonical means no
    whitespace, no newlines, keys are lexically sorted, and bytes are
    utf-8 encoded.
    """
    return json.dumps(in_obj, separators=(',',':'), sort_keys=True).encode('utf-8')


def get_cid(in_bytes):
    result = cid_sha256_hash(in_bytes)
    return result


if __name__ == "__main__":
    #
    # Get input. In future, read from file. Here, just gobble stdin.
    input_str = sys.stdin.read()

    try:
        input_blob = json.loads(input_str)
    except:
        print("Could not parse JSON", file=sys.stderr)
        raise

    if type(input_blob)==dict:
        input_blob = [input_blob]

    collection = []

    for element in input_blob:
        for key in element.keys():
            collection.append(encapsulate_dict(element[key], obj_type_map[key]))

    out_blob = {
        'data_type': 'RMD_DATA_COLLECTION',
        'contents': collection
    }

    print(json.dumps(out_blob, separators=(',',':'), sort_keys=False))
