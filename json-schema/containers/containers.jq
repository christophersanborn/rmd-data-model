reduce inputs as $item ({}; . *= $item)
| . as $objects
| (
    $objects
    | keys
  )
  as $object_codes
| reduce . as $item (
    {"_classifier": {"enum":$object_codes}};
    . *= $item
  )
| {"containers": . }
