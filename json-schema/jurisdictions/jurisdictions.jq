reduce inputs as $item ({}; . *= $item)
| . as $reports
| (
    $reports
    | keys
    | map(sub("report_";""))
  )
  as $juris_codes
| (
    $juris_codes
    | map(
        {
          "if": {
            "properties": {"target_jurisdiction": {"const": . }},
            "required": ["target_jurisdiction"]
          },
          "then": {
            "properties": {
              "details": {
                "$ref": ("#/jurisdictions/report_" + .)
              }
            },
            "required": [
              "details"
            ]
          }
        }
      )
    | {"allOf": . }
  )
  as $report_type
| reduce . as $item (
    {"_classifier": {"enum": $juris_codes }};
    . *= $item
  )
| . *= {"report_type": $report_type}
| {"jurisdictions": . }
